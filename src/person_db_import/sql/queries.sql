-- :name batch-upsert-people
-- :doc upserts a batch of people
insert into person as p (fname, lname, dob, phone) values
         :t*:people
on conflict on constraint person_unique_ix
do update set phone = excluded.phone
   where p.phone <> excluded.phone
returning true as upserted
