(ns person-db-import.etl
  (:import [com.opencsv CSVWriter])
  (:require clj-time.jdbc
            [taoensso.tufte :as tufte]
            [clojure.data.xml :as xml]
            [person-db-import.domain-spec :as ds]
            [clj-time.format :as tf]
            [clojure.spec :as spec]
            [hugsql.core :as hugsql]
            [clojure.spec.gen :as gen]
            [clojure.java.io :as io]
            [net.cgrand.xforms :as x]))

(defn parse-member
  "Given a 'member' element transforms it into a map of nested tag names and it's values"
  [xml-element]
  (->> xml-element
       :content
       (reduce (fn [agg {:keys [tag content]}]
                 (assoc agg tag (first content))) {})))

(defn try-parse-date [date]
  (try
    (tf/parse ds/date-of-birth-formatter date)
    (catch Exception ex
      nil)))

(defn parse-person
  "Given the data of 'member' element transforms it into person map and validates it. Returns a person map if it's valid or nil if it's not"
  [member-data]
  (let [res (update member-data :date-of-birth try-parse-date)]
    (if (spec/valid? ::ds/person-un res)
      res
      nil)))

(defn members-seq
  "Given a stream of xml data extracts sequence of 'member' elemetns. Returns lazy seq of sax events"
  [stream]
  (->> stream
       (io/reader)
       (xml/parse)
       :content))

(def parse-people
  (comp
   (map parse-member)
   (map parse-person)))

(hugsql/def-db-fns "person_db_import/sql/queries.sql")

(defn do-batch-upsert-people
  "Upserts a batch of person maps. Returns a vector of [upserted errors total]"
  [db coll-of-people]
  (let [valid-people (filter (comp not nil?) coll-of-people)
        total (count coll-of-people )
        prepared-data {:people (mapv #(mapv second %) valid-people)}
        upsert-stats (tufte/p :db (batch-upsert-people db prepared-data))]
    [(count upsert-stats) (- total (count valid-people)) total]))

(defn- x-reduce-fn
  ([]
   [0 0 0])
  ([val]
   val)
  ([acc val]
   (mapv + acc val)))

(tufte/defnp process-xml
  "Process whole xml stream executing batch-processing-fn to every batch of people of batch-size"
  [batch-size batch-processing-fn xml-stream]
  (let [xf (comp
            parse-people
            (x/partition batch-size batch-size nil (x/into []))
            (map batch-processing-fn))]
    (->> xml-stream
         members-seq
         (transduce xf conj)
         (reduce x-reduce-fn) ; turns out this way of using reduce is fater than adding (x/reduce) to transduce form
         )))

(def nanosecs-in-sec
  1000000000)

(defmacro profiled-in-secs
  [& body]
  `(let [res+stats# (tufte/profiled {} ~@body)]
     (update-in res+stats# [1] #(->> %
                                     :id-stats-map
                                     (map (fn [[k# v#]]
                                            [k# (/ (:time v#) (double nanosecs-in-sec))]))
                                     (into {})))))

(defn- res->map
  [[[u e t] {:keys [db ::defn_process-xml]}]]
  {:upserted u
   :errors e
   :total t
   :time-total defn_process-xml
   :time-db db})

(defn do-process
  "Convenience function to process file"
  [file db batch-size]
  (with-open [s (io/input-stream file)]
    (res->map (profiled-in-secs
               (process-xml batch-size (partial do-batch-upsert-people db) s)))))

(defn- write-header
  [w]
  (.writeNext w (into-array String ["fname" "lname" "dob" "phone"])))

(defn- write-row
  [w person]
  (.writeNext w (into-array
                 String
                 [(:firstname person)
                  (:lastname person)
                  (str (:date-of-birth person))
                  (:phone person)])))

(defn xml->csv
  [xml-file csv-file]
  (with-open [xml-stream (io/input-stream xml-file)
              csv-writer (io/writer csv-file)]
    (let [w (CSVWriter. csv-writer)
          xf (comp
              parse-people
              (filter (comp not nil?))
              (map (partial write-row w)))]
      (write-header w)
      (->> xml-stream
           members-seq
           (transduce xf conj))
      nil)))

(comment
  (def pg-db
    {:dbtype "postgresql"
     :dbname "lambdawerkz"
     :host "localhost"
     :user "pmurygin"})

  (def file "D:\\lambdawerk-backend-test\\update-file-small.xml")

  (def res8000 (do-process file pg-db 8000))
  (def res5000 (do-process file pg-db 5000))
  (def res1000 (do-process file pg-db 1000))
  (def res100 (do-process file pg-db 100))
  (def res1 (do-process file pg-db 1))

  (def full-file "D:\\lambdawerk-backend-test\\update-file.xml")

  (def res-full (do-process full-file pg-db 8000))

  (time
   (xml->csv full-file "D:\\out.csv"))
  )
