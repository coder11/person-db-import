(ns person-db-import.domain-spec
  (:require [clojure.spec :as spec]
            [clojure.spec.gen :as gen]
            [clj-time.core :as t]
            [clj-time.format :as tf]))

(spec/def ::non-empty-string (spec/and string? (comp not clojure.string/blank?)))

(spec/def :person/firstname ::non-empty-string)

(spec/def :person/lastname ::non-empty-string)

(spec/def :person/phone ::non-empty-string)

(spec/def :person/date-of-birth
  (spec/spec (partial instance? org.joda.time.DateTime)
             :gen #(gen/fmap
                    (fn [[y m d]] (t/date-time y m d))
                    (gen/tuple
                     (gen/choose 1950 2007)
                     (gen/choose 1 12)
                     (gen/choose 1 28)))))

(spec/def ::person (spec/keys :req [:person/firstname :person/lastname :person/date-of-birth :person/phone]))

(spec/def ::person-un (spec/keys :req-un [:person/firstname :person/lastname :person/date-of-birth :person/phone]))

(def date-of-birth-formatter
  (tf/formatter (tf/formatter "yyyy-MM-dd")))
