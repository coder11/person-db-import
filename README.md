# person-db-import

Library for loading xml files with information about people into postgresql table. Memory complexity is O(1). Loading done in batches of arbitrary size. On my machine (Core I5-4440 @ 3.10 GHZ, 16 GB RAM, Seagate ST1000DM003) Loading of 200 MB file takes 88 secs.

## Usage

Create a unique index and unique constraint on table person:

    >psql -d lambdawerkz -h localhost -f create-index.sql
    
To import a file use `person-db-import.etl/do-process` function

    (def pg-db
        {:dbtype "postgresql"
        :dbname "lambdawerkz"
        :host "localhost"
        :user "pmurygin"}) =>  #'person-db-import.etl/pg-db

    (def file "D:\\update-file-small.xml") => #'person-db-import.etl/file

    (do-process file pg-db 8000) =>  {:upserted 999968, :errors 49, :total 1500000, :time-total 88.406954805, :time-db 63.534344277}
    
## License

WTFPL Version 2, see LICENSE file
