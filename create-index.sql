create unique index person_unique_ix on public.person (fname, lname, dob);
alter table person add constraint person_unique_ix unique using index person_unique_ix
