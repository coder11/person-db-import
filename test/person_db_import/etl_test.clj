(ns person-db-import.etl-test
  (:use [clojure.test.check.clojure-test]
        [clojure.test])
  (:require [person-db-import.etl :refer :all]
            [person-db-import.domain-spec :as ds]
            [clojure.test.check :as tc]
            [clojure.test.check.properties :as prop]
            [clojure.spec :as spec]
            [clojure.spec.gen :as gen]
            [clojure.data.xml :as xml]
            [clj-time.format :as tf]))

(deftest date-time-parsing
  (is (nil? (try-parse-date "\\N")))
  (is (instance? org.joda.time.DateTime
                 (try-parse-date "1999-01-01"))))

(defn- tag
  ([name]
   (tag name ""))
  ([name val]
   (let [val (if (coll? val)
               (clojure.string/join val)
               val)]
     (str "<" name ">" val "</" name ">"))))

(defn- hand-craft-xml
  [coll-of-people]
  (tag "members"
       (map (fn [person]
              (tag "member"
                   [(tag "firstname" (:firstname person))
                    (tag "lastname" (:lastname person))
                    (tag "phone" (:phone person))
                    (tag "date-of-birth"
                         (tf/unparse ds/date-of-birth-formatter
                                     (:date-of-birth person)))]))
            coll-of-people)))

(defn- string->stream
  ([s] (string->stream s "UTF-8"))
  ([s encoding]
   (-> s
       (.getBytes encoding)
       (java.io.ByteArrayInputStream.))))

(defn- people->stream
  [people]
  (->> people
       (hand-craft-xml)
       (string->stream)))

(defn- serialize-deserialize-people
  [people]
  (->> people
       people->stream
       members-seq
       (transduce parse-people conj)))

(defspec serialize-deserialize-people-should-be-equal
  100
  (prop/for-all [people (gen/vector (spec/gen ::ds/person-un))]
                (= people
                   (serialize-deserialize-people people))))

(defn- mock-batch-fn
  [coll-of-people]
  (let [c (count coll-of-people)]
    [c 0 c]))

(defspec processing-of-whole-xml-should-be-valid
  100
  (prop/for-all [people (gen/vector (spec/gen ::ds/person-un))]
                (let [c (count people)]
                  (= [c 0 c]
                     (->> people
                          people->stream
                          (process-xml 10 mock-batch-fn))))))


